#include <iostream> 
#include <SFML/Graphics.hpp>
#include <fstream>
#include <sstream>
#include <GL/glew.h>

void BindBufferData(GLfloat* vertices, GLuint& vbo);
unsigned int CreateShaderProgram(const GLchar* vertexShaderText, const GLchar* fragmentShaderText);
std::string LoadFileContents(const std::string filePath);


int main()
{
    sf::ContextSettings settings;

    settings.majorVersion = 3;
    settings.minorVersion = 3;
    settings.depthBits = 24;

    sf::RenderWindow window(sf::VideoMode(800, 600), "OpenGL Made Easy", sf::Style::Default, settings);
    window.setActive(true);

    glewExperimental = true;
    GLenum result = glewInit();
    if (result != GLEW_OK)
    {
        std::cout << "Glew failed to initialize: " << glewGetErrorString(result) << std::endl;
    }

    float vertices[] = { -0.5, -0.5, 0, 0, 0.5, 0, 0.5, -0.5, 0 };

    GLuint vbo;
    GLuint vao;

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
    glEnableVertexAttribArray(0);


    std::string vertexShader = LoadFileContents("shader.vert.glsl");
    std::string fragmentShader = LoadFileContents("shader.frag.glsl");

    GLuint shaderProgram = CreateShaderProgram(vertexShader.c_str(), fragmentShader.c_str());

    glUseProgram(shaderProgram);

    while (window.isOpen())
    {
        sf::Event ev;
        while (window.pollEvent(ev))
        {
            if (ev.type == sf::Event::Closed)
            {
                window.close();
            }
        }

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glDrawArrays(GL_TRIANGLES, 0, 3);

        window.display();
    }

    return 0;
}


void BindBufferData(GLfloat* vertices, GLuint& vbo)
{
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

GLuint CreateShaderProgram(const GLchar* vertexShaderText, const GLchar* fragmentShaderText)
{
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    GLuint shaderProgram = glCreateProgram();

    glShaderSource(vertexShader, 1, &vertexShaderText, nullptr);
    glCompileShader(vertexShader);

    GLint success;
    GLchar infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, nullptr, infoLog);
        std::cout << "Vertex Shader compilation failed: " << infoLog << std::endl;
    }

    glShaderSource(fragmentShader, 1, &fragmentShaderText, nullptr);
    glCompileShader(fragmentShader);

    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, nullptr, infoLog);
        std::cout << "Fragment Shader compilation failed: " << infoLog << std::endl;
    }

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    
    glLinkProgram(shaderProgram);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return shaderProgram;
}

std::string LoadFileContents(const std::string filePath)
{
    std::ifstream file(filePath);
    std::stringstream sstream;

    if (!file.is_open())
    {
        std::cout << "Could not find file: " << filePath << std::endl;
    }

    sstream << file.rdbuf();

    return sstream.str();
}
